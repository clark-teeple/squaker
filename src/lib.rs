#[macro_use]
extern crate vst;
extern crate rand;

use vst::plugin::{Info, Plugin, Category};
use vst::buffer::AudioBuffer;
use vst::event::Event;
use vst::api::Events;
use rand::random;

#[derive(Default)]
struct Squaker {
    notes: u8
}

// vst standard interface
impl Plugin for Squaker {
    fn get_info(&self) -> Info {
        Info {
            name: "Squaker".to_string(),
            unique_id: 1337,
            inputs: 0,
            // stereo
            outputs: 2,
            category: Category::Synth,


            ..Default::default()
        }
    }

    fn process_events(&mut self, events: &Events) {
        // handle midi events
        for event in events.events() {
            match event {
                Event::Midi(ev) => {
                    // first value in data is note on/off
                    // in the midi standard, 1001nnnn is on, 1000nnnn is off
                    // will potentially update to use wmidi crate
                    // https://www.midi.org/specifications/item/table-1-summary-of-midi-message
                    match ev.data[0] {
                        144 => self.notes += 1u8,
                        128 => self.notes -= 1u8,
                        _ => (),
                    }
                    // note value is data[1]
                },
                _ => (),
            }
        }
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        if self.notes == 0 { return }
        // don't care about input buffer, would care if this was an effect plugin
        let (_, mut output_buffer) = buffer.split();
        
        // loop for stereo
        for output_channel in output_buffer.into_iter() {
            for output_sample in output_channel {
                // set output sample to random between -1.0 and 1.0
                // random would just be 0.0 to 1.0, so this subtracts 0.5 and multiplies by 2, thus creating a value between -1.0 and 1.0
                *output_sample = (random::<f32>() - 0.5f32) * 2f32;
            }
        }
    }
    
}

plugin_main!(Squaker); 

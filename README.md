First crack at a synth. Likely just a white noise generator for now.

To build:

1) `cargo build --release`
2) `git clone https://github.com/RustAudio/vst-rs` somewhere
2.1) if running on OSX, `~/<wherever you cloned vst crate>/vst-rs/osx_vst_bundler.sh Squaker target/release/libsquaker.dylib`
3) open .dll if Windows or .vst if Mac in DAW/vst host of your choice